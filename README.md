

![807d7d9a-e09f-457c-ae3c-c491d967c04a.png](https://s3.bmp.ovh/imgs/2022/03/c4b97fe755aa0284.png)


### UceEdu 职业教育版是什么


UceEdu职业教育版是一套基于uniCloud的云端一体职业教育在线培训方案解决系统，目前支持H5、APP；，支持接入个人支付。


### 为什么选择我们


1. 代码构成简单，前端到后端都是js，源码拿走就能用
1. 本项目基于serverless，永远不必担心服务器扛不住、不必为服务器开发技术不到位而操心、不必为运维操心、不必打各种补丁、不必做硬件扩容、不必管ddos攻击... 这么好的服务器，会很贵吗？答案是：uniCloud的阿里云版完全免费。而腾讯云版的价格也远低于传统云的虚拟机。
1. 运营成本低，**一次付费，终身免费使用**，免费享受后期更新以及新增功能。



### 项目构成
前端基于uni-app开发，后端基于[uniCloud](https://uniapp.dcloud.net.cn/uniCloud/README)开发

<h3 style="color:red"> 整个UceEdu，目前有2个项目，复用同一个uniCloud空间。一个项目用户端，另一个项目是管理端。以客户端云函数为准 </h3>

​

### 体验UceEdu

| 端 | 地址   | 账户 | 备注 |
| ----- | --------- | ----------- | ------- |
| H5 | [体验地址](http://uce.jimuhuatian.com/) |  自行注册           |         |
| 小程序 |   |             |     未打包    |
| APP | [下载页](https://uce.jimuhuatian.com/uce) | 自行注册            |         |
| 管理端  | [体验地址](http://uce.jimuhuatian.com/admin/) | 超管账号：admin、密码：112233445566   |         |


### 系统部署手册

- [部署前准备-项目下载](https://www.yuque.com/uce_edu/ngd5zk/myqtgf)
- [查看云服务空间ID以及URL化地址](https://www.yuque.com/uce_edu/ngd5zk/lrz7va)
- [部署前准备-短信配置](https://www.yuque.com/uce_edu/ngd5zk/bswclf)
- [部署前准备-支付配置](https://www.yuque.com/uce_edu/ngd5zk/pyhhol)
- [部署前准备-定时任务](https://www.yuque.com/uce_edu/ngd5zk/dbfn07)
- [部署前准备-登陆配置](https://www.yuque.com/uce_edu/ngd5zk/co4otg)
- [部署前准备-功能配置](https://www.yuque.com/uce_edu/ngd5zk/msb7lw)
- [项目部署与发布](https://www.yuque.com/uce_edu/ngd5zk/eqhvzi)
- [APP打包](https://www.yuque.com/uce_edu/ngd5zk/it198z)

### 系统文档

- [UceEdu使用手册](https://www.yuque.com/uce_edu/ngd5zk)
- [UceEdu更新记录](https://www.yuque.com/uce_edu/rflw4g)
- [UceEdu官方公告](https://www.yuque.com/uce_edu/thyzgp)



### 感谢与支持

- 感谢[@VK大佬](https://ext.dcloud.net.cn/publisher?id=164406)提供的VK全套开发框架
- 感谢[@uView](https://www.uviewui.com/)提供的多平台快速开发的UI框架
- ......



### 联系我们

<p style="color:red">如果您在项目使用过程中遇到什么问题或者有功能诉求，可以扫码联系我；还请不要以差评的方式提问，非常感谢！</p>

<img src="https://7463-tcb-xbyxeeqfphyrnbaae7dbd-322b87-1308187260.tcb.qcloud.la/2022/06/29/75915188-14943529-5861656475093_.pic.jpg" alt="二维码" width="200" height="400" align="middle" />